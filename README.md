Sistema de Gestión Docente - Departamento de Básicas UNLu

*******************************************************************************

Librerías utilizadas:
<br>
<br>
<b>JavaFX SDK</b>
<br>
<br>
<b>JUnit 4.12</b>
<br>
https://mvnrepository.com/artifact/junit/junit/4.12
<br>
<br>
<b>Hamcrest All 1.3</b>
<br>
https://mvnrepository.com/artifact/org.hamcrest/hamcrest-all/1.3
<br>
<br>
<b>JFXtras Labs 8.0-r6</b>
<br>
https://mvnrepository.com/artifact/org.jfxtras/jfxtras-labs/8.0-r6
<br>
<br>
<b>MySQL Connector 5.1.46</b>
<br>
https://mvnrepository.com/artifact/mysql/mysql-connector-java/5.1.46
<br>
<br>
<b>Javax Mail 1.6.1</b>
<br>
https://mvnrepository.com/artifact/com.sun.mail/javax.mail/1.6.1
<br>
<br>
<b>Apache POI 3.17</b>
<br>
https://mvnrepository.com/artifact/org.apache.poi/poi/3.17
<br>
https://mvnrepository.com/artifact/org.apache.poi/poi-ooxml/3.17

*******************************************************************************