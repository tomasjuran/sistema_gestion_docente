package modelo.usuario;


public enum Modulo {
	GENERAL,
	ROLES,
	USUARIOS,
	DOCENTES,
	INVESTIGACION,
	INFORMES,
}